import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    userRole: {
        type: Number,
        default: 1,
        required: true
    },
    isModerated: {
        type: Boolean,
        default: false,
        required: true
    }
});

const User = mongoose.model('User', userSchema);
export default User;