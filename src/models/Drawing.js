import mongoose from 'mongoose';

const drawingSchema = new mongoose.Schema({
    link: {
        type: String,
        required: true
    },
    rawDrawingInfo: String
});

const Drawing = mongoose.model('Drawing', drawingSchema);
export default Drawing;